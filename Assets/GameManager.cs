﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager get;

    [SerializeField] GameOver gameOverCanvas;

    public void Start()
    {
        get = this;
    }

    public static void GameOver() {
       get.gameOverCanvas.gameOver();
    }


}
