﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DangerTile : Floor
{
    public override void OnSteppedOn(GameObject obj)
    {
        if (obj.GetComponent<Player>()) { GameManager.GameOver(); }
    }
}
