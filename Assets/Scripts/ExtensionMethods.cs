﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Ext
{
    public static Vector2 Vec2fromAngle(float dir)
    {
        float x = Mathf.Cos(dir * Mathf.Deg2Rad);
        float y = Mathf.Sin(dir * Mathf.Deg2Rad);
        return new Vector2(x, y);
    }

    public static float angleFromVec2(Vector2 vec) {
        if (vec.y > 0) { return Vector2.Angle(Vector2.right, vec); }
        else { return Vector2.Angle(Vector2.left, vec) + 180f; }
    }

    //I can't wrap my head around this function. Found it online.
    public static bool isAngleBetween(float start, float end, float mid)
    {
        end = (end - start) < 0.0f ? end - start + 360.0f : end - start;
        mid = (mid - start) < 0.0f ? mid - start + 360.0f : mid - start;
        return (mid < end);
    }
}
