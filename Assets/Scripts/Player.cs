﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    [SerializeField] float moveSpeed; //Walk Speed
    [SerializeField] float walkControl;
    [SerializeField] float airControl;
    [SerializeField] float jumpHeight; //Height of jump in units
    [SerializeField] float gravityScale; //Acceleration

    [SerializeField] float tapGravity; //Increased gravity while ascending if jump button is released
    [SerializeField] float maxFallSpeed;
    float jumpSpeed { get { return Mathf.Sqrt(2 * jumpHeight * gravityScale); } } // calculates jump speed based off of gravity and jump height

    Rigidbody2D rb;
    Vector2 movement;

    bool tapFall;

    int doubleJumps = 1;

    bool onFloor
    {

        get
        {
            Collider2D collider = GetComponent<Collider2D>();
            RaycastHit2D[] hits = Physics2D.CapsuleCastAll((Vector2)transform.position + collider.offset, collider.bounds.size * 0.8f, CapsuleDirection2D.Vertical, 0f, Vector2.down, 0.1f);
            foreach (RaycastHit2D hit in hits)
            {
                if (hit.collider.tag == "Ground")
                {

                    return true;
                }
            }
            return false;
        }

    }

    Vector2 shortenMovementToWall(Vector2 startpos, Vector2 movement)
    {
        
        while (movement.magnitude > 0)
        {
            RaycastHit2D[] hits = Physics2D.CapsuleCastAll(startpos + GetComponent<Collider2D>().offset, GetComponent<Collider2D>().bounds.size * 1.0f, CapsuleDirection2D.Vertical, 0f, movement, movement.magnitude);
            bool collided = false;
            foreach (RaycastHit2D hit in hits)
            {
                Debug.Log(hit.point + " " + movement);
                if (hit.collider.tag == "Ground" && Vector2.Angle(movement, hit.point - (Vector2)transform.position + GetComponent<Collider2D>().offset) <= 90)
                {
                    movement = movement.normalized * Mathf.Max(0,movement.magnitude - 0.1f);
                    collided = true;
                }
            }
            if (!collided)
            {
                return movement;
            }
        }
        return movement;
    }

    Vector2 adjustMove(Vector2 movement)
    {
        Vector2 horMove = shortenMovementToWall(transform.position,new Vector2(movement.x, 0) );
        movement.x = horMove.x;
        Vector2 verMove = shortenMovementToWall((Vector2)transform.position+new Vector2(movement.x,0),new Vector2(0, movement.y));
        movement.y = verMove.y;
        return movement;

    }



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Jump()
    {
        bool doJump = false;
        if (onFloor)
        {
            doJump = true;
        }
        else
        {
            if (doubleJumps > 0) { doubleJumps--; doJump = true; }
        }
        if (doJump)
        {
            movement.y = jumpSpeed;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float horMove = Input.GetAxis("Horizontal") * moveSpeed;
        if (horMove < movement.x) { movement.x = Mathf.Max(horMove, movement.x - walkControl * moveSpeed * Time.deltaTime); }
        else { movement.x = Mathf.Min(horMove, movement.x + walkControl * moveSpeed * Time.deltaTime); }

        movement.y -= gravityScale * Time.deltaTime;


        if (Input.GetButtonDown("Jump"))
        {
            Jump();
        }
        if (movement.y > 0)
        {
            if (!Input.GetButton("Jump")) { tapFall = true; }
            if (tapFall) { movement.y -= tapGravity * Time.deltaTime; }
        }
        else { tapFall = false; }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            movement.y = -maxFallSpeed;
        }
        movement.y = Mathf.Clamp(movement.y, -maxFallSpeed, jumpSpeed);


        if (onFloor) { doubleJumps = 1; movement.y = Mathf.Max(movement.y, 0); }
        Vector2 thisFrameMove = adjustMove(movement * Time.deltaTime)            ;
       
        rb.MovePosition((Vector2)transform.position + thisFrameMove);
        //transform.Translate(movement * Time.deltaTime);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ground")
        {
            collision.collider.SendMessage("OnSteppedOn", gameObject, SendMessageOptions.DontRequireReceiver);
        }
    }

}
