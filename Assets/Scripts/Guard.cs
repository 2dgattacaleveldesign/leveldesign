﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guard : MonoBehaviour
{

    [SerializeField] float moveSpeed; //Walk Speed
    [SerializeField] float walkControl;
    [SerializeField] float airControl;
    [SerializeField] float jumpHeight; //Height of jump in units
    [SerializeField] float gravityScale; //Acceleration

    float jumpSpeed { get { return Mathf.Sqrt(2 * jumpHeight * gravityScale); } } // calculates jump speed based off of gravity and jump height

    Rigidbody2D rb;
    Vector2 movement;

    Vector2 sightOrigin { get { return (Vector2)transform.position + new Vector2(0f, 0.7f); } }


    [SerializeField] public float lookDir = 20f;
    [SerializeField] public float visionArc = 30f;
    [SerializeField] public float lookDist = 4f;

    bool onFloor
    {

        get
        {
            Collider2D collider = GetComponent<Collider2D>();
            RaycastHit2D[] hits = Physics2D.BoxCastAll((Vector2)transform.position + collider.offset, collider.bounds.size, 0f, Vector2.down, 0.1f);
            foreach (RaycastHit2D hit in hits)
            {
                if (hit.collider.tag == "Ground") { return true; }
            }
            return false;
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }



    // Update is called once per frame
    void Update()
    {
        float horMove = 0;
        if (horMove < movement.x) { movement.x = Mathf.Max(horMove, movement.x - walkControl * moveSpeed * Time.deltaTime); }
        else { movement.x = Mathf.Min(horMove, movement.x + walkControl * moveSpeed * Time.deltaTime); }

        movement.y -= gravityScale * Time.deltaTime;

        movement.y = Mathf.Clamp(movement.y, -25f, 25f);
        if (onFloor) { movement.y = Mathf.Max(movement.y, 0); }

        rb.MovePosition((Vector2)transform.position + movement * Time.deltaTime);
        //transform.Translate(movement * Time.deltaTime);

        if (checkSeePlayer()) { GameManager.GameOver(); }
    }

    bool checkSeePlayer()
    {
        Player player = FindObjectOfType<Player>();
        Vector2 vecDir = player.transform.position - transform.position;
        if (vecDir.magnitude > lookDist) { return false; }
        Debug.Log("player close enough");
        float dir = Ext.angleFromVec2(vecDir);
        Debug.Log(dir);
        if (Ext.isAngleBetween(lookDir - visionArc / 2-10f, lookDir + visionArc / 2+10f, dir))
        {
            Debug.Log("player within angle");

            RaycastHit2D[] hits = Physics2D.RaycastAll(sightOrigin, vecDir, lookDist);
            if (hits.Length > 0)
            {

                foreach (RaycastHit2D hit in hits)
                {
                    
                    if (hit.collider.tag == "Ground")
                    {
                        return false;
                    }
                    if (hit.collider.tag == "Player")
                    {
                        return true;
                    }
                }
            }
        }
        return false;

    }

    Vector2 getSightPoint(float dir)
    {
        Vector2 dirVec = Ext.Vec2fromAngle(dir);
        RaycastHit2D[] hits = Physics2D.RaycastAll(sightOrigin, dirVec, lookDist);
        if (hits.Length > 0)
        {

            foreach (RaycastHit2D hit in hits)
            {
                if (hit.collider.tag == "Ground")
                {
                    return hit.point;
                }
            }
        }
        return (Vector2)transform.position + dirVec * lookDist;

    }

    void drawLineOfSight(float dir)
    {
        Vector2 drawPoint = getSightPoint(dir) - (Vector2)transform.position;

        GL.PushMatrix();
        // Set transformation matrix for drawing to
        // match our transform
        GL.MultMatrix(transform.localToWorldMatrix);

        // Draw lines
        GL.Begin(GL.LINES);

        // Vertex colors change from red to green
        GL.Color(new Color(1f, 1f, 0f));
        // One vertex at transform position
        GL.Vertex3(0, 0.7f, 0);
        // Another vertex at edge of circle
        GL.Vertex3(drawPoint.x, drawPoint.y, 0);

        GL.End();
        GL.PopMatrix();
    }


    static Material lineMaterial;
    static void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things.
            Shader shader = Shader.Find("Hidden/Internal-Colored");
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            // Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            // Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            // Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
        }
    }

    // Will be called after all regular rendering is done
    public void OnRenderObject()
    {
        CreateLineMaterial();
        // Apply the line material
        lineMaterial.SetPass(0);

        for (int i = 0; i < 10; i++)
        {
            float dir = lookDir - visionArc / 2f + (visionArc * i / 10);
            drawLineOfSight(dir);
        }



    }

}
