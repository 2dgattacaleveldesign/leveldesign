﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameOver : MonoBehaviour
{
    [SerializeField] Image background;
    [SerializeField] TextMeshProUGUI text;

    bool gameOvering = false;
    // Start is called before the first frame update
    void Start()
    {
        text.rectTransform.localScale = new Vector3(0f, 0f, 1f);
    }
    public void gameOver()
    {
        StartCoroutine(GameOverTimer());
    }

    // Update is called once per frame
    void Update()
    {
        if (gameOvering)
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, 0.25f, 2f * Time.fixedDeltaTime);
            background.color = Color.Lerp(background.color, new Color(1, 0, 0, 0.5f), 2f * Time.fixedDeltaTime);
            text.rectTransform.localScale = Vector3.Lerp(text.rectTransform.localScale, new Vector3(1f, 1f, 1f), 2f * Time.fixedDeltaTime);
        }
    }

    IEnumerator GameOverTimer() {
        gameOvering = true;
        yield return new WaitForSecondsRealtime(2f);
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
