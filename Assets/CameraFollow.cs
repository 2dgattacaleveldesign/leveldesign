﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] Rigidbody2D follow;

    Vector2 lastFollowMovement;
    Vector2 followMovement;
    

    // Update is called once per frame
    void Update()
    {
        followMovement = ((Vector2)follow.transform.localPosition - lastFollowMovement);
        if(followMovement.magnitude > 0.05f) { followMovement = followMovement.normalized; }

        Vector3 tarPos = follow.transform.localPosition;
        tarPos = new Vector3(tarPos.x, tarPos.y + 2.5f, -10);
        tarPos += (Vector3)followMovement;
        transform.localPosition =Vector3.Lerp(transform.localPosition, tarPos, 4f * Time.deltaTime);

        lastFollowMovement = follow.transform.localPosition;
    }
}
